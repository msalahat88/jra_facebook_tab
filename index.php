<?php
require_once 'header.php';

?>

    <div class="width">

            <h3 class="searchfor">ابحث عن: </h3>

    </div>

    <div style="margin-top: 0px;">
        <?php
        $model_array = get_row('E');
        if(!empty($model_array['التخصص'])){
            $counter =0;
            foreach ($model_array['التخصص'] as $index=>$item) {
                if(trim($item) == 'الحدائق المائية' || trim($item) == 'المنتجعات'){
                    continue;
                }
                if($counter%2==0){
                    echo '<div class="row">';
                }
                $class ='';
                if($counter %2){
                    $class= 'right-bg';
                }

                ?>
                <a href="locations_items.php?activities=<?=$item?>" class="<?=$class?>">
                    <div class="half" >
                        <div style="background-image: url('images/jra/<?=trim($item)?>.png');height:215px;width:338px;background-repeat: no-repeat;">
                        </div>
                        <?php
                        if(trim($item) == 'المدن الترفيهية'){
                            $item = 'مدن تسلية وترويح سياحي';
                        }
                        echo '<h4>'.$item.'</h4>';

                        ?>
                    </div>
                </a>
                <?php
                if($counter%2!=0){
                    echo '</div>';
                }
                $counter++;
            }
        }
        ?>
    </div>

<?php
require_once 'footer.php';
?>