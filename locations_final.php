
<?php
require_once 'header_last_page.php';
?>
<style>
    .container{
        position: relative;
    }
    .city_theme{
        background-image: url("images/jra/city-background.png");
        background-size: 100% 100%;
        background-repeat: no-repeat;
        position: relative;
        z-index: 99; width: 255px; height: 126px; padding:20px 0 0; position:absolute;
        top:75px;
    }
    .line-name{background-image: url("images/jra/city-line.png");background-repeat: no-repeat; width:100%; height: 10px; background-position:center; display: block; }
    .city{font-family: 'Conv_Hacen Tunisia Bold'; font-size: 45px; text-align: center; line-height: 52px;}
    .place{font-family: 'Conv_Hacen Tunisia Lt'; font-size: 20px; text-align: center; color: #c6c6c6;}
    .padding-box{padding:40px;font-family: 'Conv_Hacen Tunisia Lt'; color: #4e4e4e; font-size: 25px;}
    .padding-box .border{border-bottom:1px solid #cbe1ee;}
    .padding-box .border h4:before{ content: ' • '; color: #368cbc;}
    .padding-box .border h5:before{ content: ' '; color: #368cbc; background-image: url("images/jra/add-icon.png"); background-repeat: no-repeat; width: 24px; height: 24px; display: inline-block; margin-left: 5px; }
    .place-box{ width: 47%; float: right;}
    .left-box{ float: left;}
    body{color:white};
</style>
<script>
    $(document).ready(function(){
        $(".items_prop").hide();
        $('.show_more_btn').remove();
        $(".items_prop").slice(0,16).show();
        var btns ='';
        if($(".items_prop").slice(Number(16),Number(32)).length >0) {
            btns +='<button data-from="' + Number(16) + '" data-to="' + Number(32) + '" class="show_more_btn btn btn-primary">اظهر المزيد</button>';
        }

        $('.all_items').append('<div class="col-sm-3 col-sm-offset-4">' +
            btns +
            '</div>');
        $('body').on('click','.show_more_btn',function(){
            var $from = $(this).data('from');
            var $to = $(this).data('to');
            var btns = '';
            $(".items_prop").hide();
            var offset = 'col-sm-offset-4';
            if($(".items_prop").slice(Number($from-16),Number($to-16)).length >0) {
                btns +='<button data-from="' + Number($from - 16) + '" data-to="' + Number($to - 16) + '" class="show_more_btn btn btn-primary">اظهر اقل</button>';
            }else{
                offset = 'col-sm-offset-3';
            }
            if($(".items_prop").slice(Number($from+16),Number($to+16)).length >0) {
                btns +='<button data-from="' + Number($from + 16) + '" data-to="' + Number($to + 16) + '" class="show_more_btn btn btn-primary">اظهر المزيد</button>';
            }else{
                offset = 'col-sm-offset-3';
            }
            $(".items_prop").slice($from, $to).show();
            $('.show_more_btn').remove();
            $('.all_items').append('<div class="col-sm-4 '+offset+'">' + btns + '</div>');

        })
    })
</script>
<?php
$row_items = '';
$row_items = '';
if(isset($_GET['activities']) && isset($_GET['city'])){
    $activities = $_GET['activities'];
    $city = $_GET['city'];
    $model = get_row_and_arrange('D','B',$city,$activities);

}
?>
<div class="col-sm-4 pull-right" style="margin-bottom: 20px;">

    <a href="javascript:history.go(-1)" class="btn btn-primary">BACK</a>
</div>

    <div class="width" >
        <div style="position: absolute;top:20px; left:0;"><img src="images/jra/image1.png" /></div>

        <div style="position: absolute;bottom:0; right:0;"><img src="images/jra/image2.png" /></div>
    </div>
<div class="padding">

    <div class="width" >

        <div class="pull-right city_theme">
            <div class="city"><?=$city?></div>
            <div class="line-name"></div>
            <?php
            if(trim($activities) == 'المدن الترفيهية'){
                $activities = 'مدن تسلية وترويح سياحي';
            }
            ?>
            <div class="place"><?=$activities?></div>
        </div>


        <div class="width all_items" style="position:relative;background: rgba(255, 255, 255, .7); color:black; margin-top: 120px">
            <div style="background-color: #fff; width:96%; margin: 2% auto;">
                <div class="padding-box">
            <?php
            if(!empty($model)){


                $counter =0;
                foreach ($model as $index=>$items) {
                    foreach ($items as $item) {
                        $class= '';
                        if($counter%2==0){

                            echo '<div class="row items_prop">';
                        }
                        if($counter%2 !=0){
                            $class= 'left-box';
                        }
                        ?>
                        <div class="place-box border <?=$class?>" >
                            <h4><?=$item?></h4>
                            <h5><?=$index?></h5>

                        </div>
                        <?php
                        if($counter%2!=0){
                            echo '</div>';

                        }
                        $counter++;

                    }
                }
            }
            ?>
        </div></div></div>

    </div></div>

<?php
require_once 'footer.php';
?>